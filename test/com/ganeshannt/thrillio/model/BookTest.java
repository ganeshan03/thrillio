package com.ganeshannt.thrillio.model;

import com.ganeshannt.thrillio.constants.BookGenre;
import com.ganeshannt.thrillio.service.BookmarkService;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class BookTest {

    @Test
    public void isKidFriendlyEligible() {
        //Test 1 - PHILOSOPHY in genre  --> false
        Book book = BookmarkService.getInstance().createBook(4000, "Walden", 1854, "Wilder Publications", new String[]{"Henry", "David", "Thoreau"}, BookGenre.PHILOSOPHY, 4.3);
        assertFalse("It has 'PHILOSOPHY' genre", book.isKidFriendlyEligible());

        //Test 2 - SELF-HELP in genre  --> false
        book = BookmarkService.getInstance().createBook(4000, "Walden", 1854, "Wilder Publications", new String[]{"Henry", "David", "Thoreau"}, BookGenre.SELF_HELP, 4.3);
        assertFalse("It has 'SELF-HELP' genre", book.isKidFriendlyEligible());

        //Test 3 - Happy path scenario  --> true
        book = BookmarkService.getInstance().createBook(4000, "Walden", 1854, "Wilder Publications", new String[]{"Henry", "David", "Thoreau"}, BookGenre.TECHNICAL, 4.3);
        assertTrue("It is a happy path scenario but still it fails", book.isKidFriendlyEligible());
    }
}