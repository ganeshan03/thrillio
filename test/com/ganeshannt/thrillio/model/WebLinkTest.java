package com.ganeshannt.thrillio.model;

import com.ganeshannt.thrillio.service.BookmarkService;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class WebLinkTest {

    @Test
    public void isKidFriendlyEligible() {
        //Test 1 - porn in title --> false
        WebLink webLink = BookmarkService.getInstance().createWebLink(2000, "Taming porn, Part 2", "http://www.javaworld.com/article/2072759/core-java/taming-tiger--part-2.html", "http://www.javaworld.com");
        assertFalse("It has 'PORN' word in title", webLink.isKidFriendlyEligible());

        //Test 2 - sex in URL --> false
        webLink = BookmarkService.getInstance().createWebLink(2000, "Taming hi, Part 2", "http://www.javaworld.com/article/2072759/sex/taming-tiger--part-2.html", "http://www.javaworld.com");
        assertFalse("It has 'SEX' word in url", webLink.isKidFriendlyEligible());

        //Test 3 - adult in host --> false
        webLink = BookmarkService.getInstance().createWebLink(2000, "Taming hi, Part 2", "http://www.javaworld.com/article/2072759/core-java/taming-tiger--part-2.html", "http://www.adult.com");
        assertFalse("It has 'ADULT' word in host", webLink.isKidFriendlyEligible());

        //Test 4 - adult in url not in host --> true
        webLink = BookmarkService.getInstance().createWebLink(2000, "Taming hi, Part 2", "http://www.javaworld.com/article/2072759/adult/taming-tiger--part-2.html", "http://www.javaworld.com");
        assertTrue("it is kid-friendly URL but still it fails", webLink.isKidFriendlyEligible());

        //Test 4 - happy path scenario --> true
        webLink = BookmarkService.getInstance().createWebLink(2000, "Taming hi, Part 2", "http://www.javaworld.com/article/2072759/core-java/taming-tiger--part-2.html", "http://www.javaworld.com");
        assertTrue("it is kid-friendly URL but still it fails", webLink.isKidFriendlyEligible());
    }
}