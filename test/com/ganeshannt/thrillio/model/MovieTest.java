package com.ganeshannt.thrillio.model;

import com.ganeshannt.thrillio.constants.MovieGenre;
import com.ganeshannt.thrillio.service.BookmarkService;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class MovieTest {

    @Test
    public void isKidFriendlyEligible() {
        //Test 1 - HORROR in genre  --> false
        Movie movie = BookmarkService.getInstance().createMovie(3000, "Citizen Kane", 1941, new String[]{"Orson Welles", "Joseph Cotten"}, new String[]{"Orson", "Welles"}, MovieGenre.HORROR, 8.5);
        assertFalse("It has 'HORROR' genre", movie.isKidFriendlyEligible());

        //Test 2 - THRILLERS in genre  --> false
        movie = BookmarkService.getInstance().createMovie(3000, "Citizen Kane", 1941, new String[]{"Orson Welles", "Joseph Cotten"}, new String[]{"Orson", "Welles"}, MovieGenre.THRILLERS, 8.5);
        assertFalse("It has 'THRILLERS' genre", movie.isKidFriendlyEligible());

        //Test 3 - Happy path scenario  --> true
        movie = BookmarkService.getInstance().createMovie(3000, "Citizen Kane", 1941, new String[]{"Orson Welles", "Joseph Cotten"}, new String[]{"Orson", "Welles"}, MovieGenre.CLASSICS, 8.5);
        assertTrue("It has 'CLASSICS' genre", movie.isKidFriendlyEligible());

    }
}