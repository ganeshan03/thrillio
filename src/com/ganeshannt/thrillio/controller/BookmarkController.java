package com.ganeshannt.thrillio.controller;

import com.ganeshannt.thrillio.model.Bookmark;
import com.ganeshannt.thrillio.model.User;
import com.ganeshannt.thrillio.service.BookmarkService;

public class BookmarkController {
    private static BookmarkController bookmarkController = new BookmarkController();

    private BookmarkController() {
    }

    public static BookmarkController getInstance() {
        return bookmarkController;
    }

    public void saveBookmark(User user, Bookmark bookmark) {
        BookmarkService.getInstance().saveUserBookmark(user, bookmark);
    }

    public void setKidFriendlyStatus(User user, String kidFriendlyStatus, Bookmark bookmark) {
        BookmarkService.getInstance().setKidFriendlyStatus(user,kidFriendlyStatus,bookmark);
    }

    public void share(User user, Bookmark bookmark) {
        BookmarkService.getInstance().share(user,bookmark);
    }
}
