package com.ganeshannt.thrillio.model;

import com.ganeshannt.thrillio.constants.BookGenre;
import com.ganeshannt.thrillio.partner.Shareable;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;

public class Book extends Bookmark implements Shareable {
    private int publicationYear;
    private String publisher;
    private String[] authors;
    private String genre;
    private double amazonRating;


    public int getPublicationYear() {
        return publicationYear;
    }

    public void setPublicationYear(int publicationYear) {
        this.publicationYear = publicationYear;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String[] getAuthors() {
        return authors;
    }

    public void setAuthors(String[] authors) {
        this.authors = authors;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public double getAmazonRating() {
        return amazonRating;
    }

    public void setAmazonRating(double amazonRating) {
        this.amazonRating = amazonRating;
    }

    @Override
    public String toString() {
        return "Book{" +
                "publicationYear=" + publicationYear +
                ", publisher='" + publisher + '\'' +
                ", authors=" + Arrays.toString(authors) +
                ", genre='" + genre + '\'' +
                ", amazonRating=" + amazonRating +
                '}';
    }

    @Override
    public boolean isKidFriendlyEligible() {
        if (this.getGenre().equals(BookGenre.PHILOSOPHY) || this.getGenre().equals(BookGenre.SELF_HELP)) {
            return false;
        }
        return true;
    }

    @Override
    public String getItemData() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<item>");
        stringBuilder.append("<title>").append(getTitle()).append("</title>");
        stringBuilder.append("<author>").append(StringUtils.join(getAuthors(),",")).append("</author>");
        stringBuilder.append("<publicationYear>").append(getPublicationYear()).append("</publicationYear>");
        stringBuilder.append("<publisher>").append(getPublisher()).append("</publisher>");
        stringBuilder.append("<genre>").append(getGenre()).append("</genre>");
        stringBuilder.append("<amazonRating>").append(getAmazonRating()).append("</amazonRating>");
        stringBuilder.append("</item>");
        return stringBuilder.toString();
    }
}
