package com.ganeshannt.thrillio.model;

import com.ganeshannt.thrillio.partner.Shareable;
import org.apache.commons.lang3.StringUtils;

public class WebLink extends Bookmark implements Shareable {
    private String url;
    private String host;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    @Override
    public String toString() {
        return "WebLink{" +
                "url='" + url + '\'' +
                ", host='" + host + '\'' +
                '}';
    }

    @Override
    public boolean isKidFriendlyEligible() {
        String[] suspectedKeywords = new String[]{"sex", "adult", "porn", "xvideos"};
        for (String suspectedKeyword : suspectedKeywords) {
            if (!suspectedKeyword.equals("adult")) {
                if (StringUtils.containsIgnoreCase(this.getUrl(), suspectedKeyword)) {
                    return false;
                }
            }
            if (StringUtils.containsIgnoreCase(this.host, suspectedKeyword) || StringUtils.containsIgnoreCase(this.getTitle(), suspectedKeyword)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public String getItemData() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<item>");
        stringBuilder.append("<url>").append(getUrl()).append("</url>");
        stringBuilder.append("<host>").append(getHost()).append("</host>");
        stringBuilder.append("</item>");
        return stringBuilder.toString();
    }
}
