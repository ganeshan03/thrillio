package com.ganeshannt.thrillio;


import com.ganeshannt.thrillio.data.DataStore;
import com.ganeshannt.thrillio.data.View;
import com.ganeshannt.thrillio.model.Bookmark;
import com.ganeshannt.thrillio.model.User;
import com.ganeshannt.thrillio.service.BookmarkService;
import com.ganeshannt.thrillio.service.UserService;

public class Launch {

    private static User[] users;
    private static Bookmark[][] bookmarks;

    public static void main(String[] args) {
        loadData();
        start();
    }


    private static void loadData() {
        System.out.println("1. loading sample data ...");
        DataStore.loadData();
        users = UserService.getInstance().getUsers();
        bookmarks = BookmarkService.getInstance().getBookmarks();
//        System.out.println("2. printing data ...");
//        printUserData();
//        printBookmarkData();
    }

    private static void start() {
//        System.out.println("2. User bookmarking ...");
        for (User user : users) {
            View.browse(user, bookmarks);
        }
    }

    private static void printBookmarkData() {
        for (Bookmark[] bookmarkList : bookmarks) {
            for (Bookmark bookmark : bookmarkList) {
                System.out.println(bookmark);
            }
        }
    }

    private static void printUserData() {
        for (User user : users) {
            System.out.println(user);
        }
    }
}
