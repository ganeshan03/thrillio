package com.ganeshannt.thrillio.repository;

import com.ganeshannt.thrillio.data.DataStore;
import com.ganeshannt.thrillio.model.Bookmark;
import com.ganeshannt.thrillio.model.UserBookmark;

public class BookmarkRepository {

    public Bookmark[][] getBookmarks() {
        return DataStore.getBookmarks();
    }

    public void saveUserBookmark(UserBookmark userBookmark) {
        DataStore.add(userBookmark);
    }
}
