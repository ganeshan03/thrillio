package com.ganeshannt.thrillio.repository;

import com.ganeshannt.thrillio.data.DataStore;
import com.ganeshannt.thrillio.model.User;

public class UserRepository {

    public User[] getUsers() {
        return DataStore.getUsers();
    }
}
