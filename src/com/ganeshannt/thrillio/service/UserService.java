package com.ganeshannt.thrillio.service;

import com.ganeshannt.thrillio.model.User;
import com.ganeshannt.thrillio.repository.UserRepository;

public class UserService {
    private static final UserService userService = new UserService();
    private static UserRepository userRepository = new UserRepository();

    private UserService() {
    }

    public static UserService getInstance() {
        return userService;
    }

    public User createUser(long id, String email, String password, String firstName, String lastName, int gender, String userType) {
        User user = new User();
        user.setId(id);
        user.setEmail(email);
        user.setPassword(password);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setUserType(userType);
        user.setGender(gender);
        return user;
    }

    public User[] getUsers() {
        return userRepository.getUsers();
    }
}
