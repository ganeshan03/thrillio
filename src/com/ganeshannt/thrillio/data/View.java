package com.ganeshannt.thrillio.data;

import com.ganeshannt.thrillio.constants.KidFriendlyStatus;
import com.ganeshannt.thrillio.constants.UserType;
import com.ganeshannt.thrillio.controller.BookmarkController;
import com.ganeshannt.thrillio.model.Bookmark;
import com.ganeshannt.thrillio.model.User;
import com.ganeshannt.thrillio.partner.Shareable;

public class View {

    public static void browse(User user, Bookmark[][] bookmarks) {
        System.out.println(user.getFirstName() + " is browsing items ...");

        int bookmarkcount = 0;

        for (Bookmark[] bookmarkList : bookmarks) {
            for (Bookmark bookmark : bookmarkList) {
                if (bookmarkcount < DataStore.USER_BOOKMARK_LIMIT) {
                    boolean isBookmarked = getBookmarkDecision(bookmark);
                    if (isBookmarked) {
                        bookmarkcount++;
                        BookmarkController.getInstance().saveBookmark(user, bookmark);
                        System.out.println("New item bookmarked ----- " + bookmark);
                    }
                }

                if (user.getUserType().equals(UserType.EDITOR) || user.getUserType().equals(UserType.CHIEF_EDITOR)) {
                    //        mark it as kid-friendly
                    if (bookmark.isKidFriendlyEligible() && bookmark.getKidFriendlyStatus().equals(KidFriendlyStatus.UNKNOWN)) {
                        String kidFriendlyStatus = getKidFriendlyStatusDecision(bookmark);
                        if (!kidFriendlyStatus.equals(KidFriendlyStatus.UNKNOWN)) {
                            BookmarkController.getInstance().setKidFriendlyStatus(user, kidFriendlyStatus, bookmark);
                            System.out.println("kidFriendlyStatus " + kidFriendlyStatus + " -- bookmark item --> " + bookmark);
                        }
                    }

                    if (bookmark.getKidFriendlyStatus().equals(KidFriendlyStatus.APPROVED) && bookmark instanceof Shareable) {
                        boolean isShared = getShareDecision();
                        if (isShared) {
                            BookmarkController.getInstance().share(user, bookmark);
                        }
                    }
                }
            }
        }
/*
        for (int i = 0; i < DataStore.USER_BOOKMARK_LIMIT; i++) {
            int typeoffset = (int) (Math.random() * DataStore.BOOKMARK_TYPES_COUNT);
            int bookmarkoffset = (int) (Math.random() * DataStore.BOOKMARKS_PER_TYPE);
            Bookmark bookmark = bookmarks[typeoffset][bookmarkoffset];
            BookmarkController.getInstance().saveBookmark(user, bookmark);
        } */
    }

    //TODO:Get input from user using console window
    private static boolean getShareDecision() {
        return Math.random() < 0.5;
    }

    private static String getKidFriendlyStatusDecision(Bookmark bookmark) {
        double randomVal = Math.random();
        return randomVal < 0.4 ? KidFriendlyStatus.APPROVED : randomVal < 0.8 ? KidFriendlyStatus.REJECTED : KidFriendlyStatus.UNKNOWN;
    }

    private static boolean getBookmarkDecision(Bookmark bookmark) {
        return Math.random() < 0.5;
    }
}
